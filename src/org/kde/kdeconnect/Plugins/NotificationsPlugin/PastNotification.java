package org.kde.kdeconnect.Plugins.NotificationsPlugin;

final class PastNotification {
    private final String key;
    private String title;
    private String body;

    public boolean hasSameContent(String title, String body) {
        return this.title != null && this.body != null && this.title.equals(title) && this.body.equals(body);
    }

    public void update(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public String getKey() {
        return key;
    }

    PastNotification(String key) {
        this.key = key;
    }
}
