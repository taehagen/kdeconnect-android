package org.kde.kdeconnect.Plugins.NotificationsPlugin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.preference.Preference;
import androidx.preference.PreferenceManager;
import androidx.preference.PreferenceScreen;

import org.kde.kdeconnect.UserInterface.PluginSettingsFragment;

public class NotificationSettingsFragment extends PluginSettingsFragment {

    private final static String SILENCE_OLD_NOTIFICATIONS = "notification_silence_old";
    private final static String SILENCE_EQUAL_UPDATES = "notification_silence_equal_updates";

    public static NotificationSettingsFragment newInstance(@NonNull String pluginKey) {
        NotificationSettingsFragment fragment = new NotificationSettingsFragment();
        fragment.setArguments(pluginKey);

        return fragment;
    }

    public static boolean areOldNotificationsSilent(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SILENCE_OLD_NOTIFICATIONS, true);
    }

    public static boolean areSilentUpdatesEnabled(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SILENCE_EQUAL_UPDATES, true);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);

        PreferenceScreen preferenceScreen = getPreferenceScreen();
        final Preference filterApps = preferenceScreen.findPreference("filter_apps");

        filterApps.setOnPreferenceClickListener(preference -> {
            Intent intent = new Intent(getActivity(), NotificationFilterActivity.class);
            startActivity(intent);
            return true;
        });
    }
}
